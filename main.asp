<!-- <%@Language=VBScript%> -->
<!-- #include virtual="/inc/db_conn.asp" -->
<!-- <% news_id="e-learning" %> -->
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml"> -->
<head>
  <meta http-equiv="Content-Language" content="en-us">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Omron Solutions | <%=ctry%> | Omron IA </title>
  <meta name="KEYWORDS" content="omron solutions,automation e-learning,industrial automation e-learning,factory automation e-learning,omron automation e-learning">
  <meta name="DESCRIPTION" content="Omron Automation e-Learning is a place of education where you can learn online about Omron's various factory-automation devices. Access these e-Learning courses and study at any time anywhere, about the devices you want to know about when you need to.">
  <!-- #include file="../inc/style.inc" -->
  <link rel="stylesheet" type="text/css" media="all" href="common/css/common.css" />
  <link rel="stylesheet" type="text/css" media="print" href="common/css/print.css" />

  <!-- --------------------- DEV CSS START HERE --------------------- -->
  <link rel="stylesheet" type="text/css" media="all" href="new_css/solutions.css" />

  <!-- --------------------- DEV CSS END HERE --------------------- -->
</head>
<body id="www-ia-omron-com" class="tmplB01">
  <div class="omron-ia">
    <p class="non-visual"><a name="page-top" id="page-top">Page top</a></p>
    <!-- #include file="../inc/banner.inc" -->
    <!-- #include file="../inc/menu.inc" -->
  <div class="bread-crumb-area">
    <ul>
      <li>
        <a href="/">Home</a>
        <span class="separator">&gt;</span>
      </li>
      <li>
        <strong>Solutions</strong>
      </li>
    </ul>
  </div>


  <!-- --------------------- DEV START HERE --------------------- -->
  <div class="solution-page">
    <h1>Solutions</h1>
    <div class="row items">
      <div class="col-3 item">
        <span class="tag new">NEW!</span>
        <a href="/packaging/"><img src="images/solutions/thumb-01.png"></a>
        <article>
          <a class="title" href="/packaging/">Meeting Demands For Quality And Diversified Packaging Needs</a>
          <p>Having an efficient packaging line that optimizes production starts with designing it right. OMRON Packaging Machine Control Solutions are designed to help Machine Makers maximize equipment effectiveness and accelerate machine design to drive manufacturing efficiency for manufacturers. Discover how you can increase operational efficiency for your business today.</p>
        </article>
        <a class="link" href="/packaging/">READ MORE</a>
      </div>
      <div class="col-3 item">
        <span class="tag new">NEW!</span>
        <a href="/robotics/mobilerobot"><img src="images/solutions/thumb-03.png"></a>
        <article>
          <a class="title" href="/robotics/mobilerobot">Transforming Manufacturing And Logistics Industries With Mobile Robot Solution</a>
          <p>Designed to dramatically increase productivity in manufacturing and logistics operations, our mobile robots automate the most demanding yet repetitive 24/7 tasks. This drastically reduces manual labour work and allows your employees to be more efficient, focusing on higher-level tasks that require more complex human skills.</p>
        </article>
        <a class="link" href="/robotics/mobilerobot">READ MORE</a>
      </div>
      <div class="col-3 item">
        <span class="tag new">NEW!</span>
        <a href="/solutions/application_solutions/sysmac_traceability/main.asp"><img src="images/solutions/thumb-02.png"></a>
        <article>
          <a class="title" href="/solutions/application_solutions/sysmac_traceability/main.asp">How OMRON Traceability Solution Helps To Secure Your Data And Safeguard Your Consumer</a>
          <p>Capable of tracing every step of the manufacturing process, specifically quality assurance and specification data, OMRON Traceability Solution prevents data loss with seamless tracking and tracing of data, that results in easier and lower maintenance that can be done remotely at the global level, ultimately reducing the Total Cost of Ownership (TCO). </p>
        </article>
        <a class="link" href="/solutions/application_solutions/sysmac_traceability/main.asp">READ MORE</a>
      </div>
      <div class="col-3 item">
        <a href="/panelsolutions/"><img src="images/solutions/thumb-04.png"></a>
        <article>
          <a class="title" href="/panelsolutions/">Innovation In Control Panel Solutions Revolutionise The Way You Work</a>
          <p>Control Panels are the heart of manufacturing sites. Learn how you can increase operational efficiency and experience cost savings with OMRON’s New Value in Panel Solutions.</p>
        </article>
        <a class="link" href="/panelsolutions/">READ MORE</a>
      </div>
      <div class="col-3 item">
        <a href="/io-link/"><img src="images/solutions/thumb-05.png"></a>
        <article>
          <a class="title" href="/io-link/">How Do You Locate A Faulty Sensor In A Big Production Plant?</a>
          <p>Imagine a production plant with thousands of machines. Downtime due to one faulty sensor will be a nightmare for any maintenance engineer, with troubleshooting taking from hours to weeks. How can we quickly and easily identify the exact point of failure? Discover IO-Link, the future of communications at the sensor level.</p>
        </article>
        <a class="link" href="/io-link/">READ MORE</a>
      </div>
      <div class="col-3 item">
        <a href="/solutions/sysmacstyle/index.html"><img src="images/solutions/thumb-06.png"></a>
        <article>
          <a class="title" href="/solutions/sysmacstyle/index.html">SYSMAC: More Than Just An Automation Platform</a>
          <p>Latest machine automation platform from OMRON with integration throughout - From Motion, Sequencing, Safety, Networking to Vision Inspection.</p>
        </article>
        <a class="link" href="/solutions/sysmacstyle/index.html">READ MORE</a>
      </div>
      <div class="col-3 item">
        <a href="/solutions/product/colormarksensors/"><img src="images/solutions/thumb-07.png"></a>
        <article>
          <a class="title" href="/solutions/product/colormarksensors/">Overcome Packaging Machine Limitations In Detecting Subtle Differences In Glossy & Colorful Packaging</a>
          <p>With increasingly consumer demands and competition, packaging materials and designs have become more diversified to attract consumers’ attention. This presents new challenges for the packaging industry, with the increasing need to accurately detect color marks on all sorts of materials in order to keep production plants running stably.</p>
        </article>
        <a class="link" href="/solutions/product/colormarksensors/">READ MORE</a>
      </div>
    </div>
  </div>

      <!-- === [top-page-link-area] === -->
      <!-- #include file="../inc/footer.inc" -->

  </div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
  var hw = 0;
  function rowHeight(i,item){
    var n = i+1,c=n%3==0? 2 : n%3-1;
      for(var x = i-c;i>=x;i--){
        item.eq(i).height(hw);
      }
    hw = 0;

  }

    function autoHeight(item){
      var w = $(window).width(),
          length = item.length;
      item.each(function(i){
        $(this).removeAttr('style');
        var h = $(this).height(),n=i+1;
        if(h > hw){
          hw = h;
        }
        if(n%3 == 0|| n ==length){
          rowHeight(i,item);
        }
      })
    }
    var item = $('.items').find('article');
    autoHeight(item);
</script>
</body>
</html>
