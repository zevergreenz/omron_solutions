// #############################################################################
// Global Navigation
// #############################################################################

// change background color
$(function(){
    var url = document.URL.split('/');
    if ( url[3] == 'selection_guide' ) {
        $('#products').attr('id', 'current-navi');
    }
    if ( url[3] == 'new' ) {
        $('#products').attr('id', 'current-navi');
    }
    if ( url[3] == 'rohs' ) {
        $('#oep_search').attr('id', 'current-navi');
    }
    if ( url[3] == 'replacement' ) {
        $('#oep_search').attr('id', 'current-navi');
    }
    if ( url[3] == 'stock_info' ) {
        $('#oep_search').attr('id', 'current-navi');
    }
    if ( url[3] == 'search' ) {
        $('#products').attr('id', 'current-navi');
    }
    if ( url[3] == '' ) {
        $('#home').attr('id', 'current-navi');
    } else {
        $('.topMenu li').each(function() {
            var menu_id = $(this).attr('id');
            if (menu_id == url[3]) {
                $(this).attr('id', 'current-navi');
            return false;
            }
        });
    }
});


// #############################################################################
// Header / Footer
// #############################################################################
// Login URL
$(function(){
    var url = document.URL.split('/');
    var url2 = url[2];
    $("a[href = 'admin/login.asp']").attr("href", "https://" +url2+ "/admin/login.asp");
});


// ############################
// function : login URL hide show 
// add : 20140204
// ############################

//$(function(){
//	var setLogin = new iniLogin();
//	if (setLogin.truth) {
//        $("a.loginlink").remove();
//    }
//});
