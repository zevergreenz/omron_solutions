﻿var userCont = {};
var loginCheck = {};
var setAccount = {};
var loginCall = {};


$(function(){
	
	/*
	Account Service List
	------------------------------------------------------------------------------*/
	userCont.targetObj = $(".user-contents-list");
	userCont.init = function(){
		$(".user").toggle(function(e){
			$(userCont.targetObj).show(0);
		},
		function(e){
			$(userCont.targetObj).hide(0);
		})
	}
	
	/*
	Login Check
	------------------------------------------------------------------------------*/
	loginCheck = function(funcObj){
		var suc = function(returnSuc){
				funcObj(returnSuc,1);
		}
		var err = function(returnErr){
				funcObj(returnErr,0);
		}
		var ajaxobj = {
			"uri":"/teeda.ajax?component=webMembers&action=isLogin",
			"type":"json",
			"method":"get",
			"fnSuc":suc,
			"fnErr":err
		}
		doXhr(ajaxobj);
	}

	/*
	Set Account
	------------------------------------------------------------------------------*/
	setAccount = function(returnObj,status) {
		if(status && returnObj.isLogin){
			$('.username-area .devider .user span').html(returnObj.familyName+' '+returnObj.givenName);
			userCont.init();
			$('.username-area .devider .user').show(); //show use name
		} else {
			$('.username-area .devider .loginlink').show(); // show login link
		}
	}
	
	
	/*
	Logout API Call
	------------------------------------------------------------------------------*/
	
	logoutCall = function(funcObj){
		var suc = function(returnSuc){
				window.location.reload();
		}
		var err = function(returnErr){
				window.location.reload();
		}
		var ajaxobj = {
			"uri":"/teeda.ajax?component=webMembers&action=logout",
			"type":"json",
			"method":"get",
			"fnSuc":suc,
			"fnErr":err
		}
		doXhr(ajaxobj);
	}
	
	
	
	loginCheck(setAccount);

});


/*=========================================================================
 general method
=========================================================================*/

/*
https check and replace
------------------------------------------------------------------------------*/
var httpsState = true;
function ad(texturl) {
	if (!httpsState) {
		//texturl = texturl.replace("https://", "http://");
		texturl = "/error/ssl_error.html";
	}
	location.href = texturl;
}
function checkHttps(){
	httpsState = false;
}


/*
logout
------------------------------------------------------------------------------*/
function logout(){
	logoutCall();
	//window.location.reload();
}


/*
loginAction
------------------------------------------------------------------------------*/
function loginAction(){
	var _redirectPath = getRedirectPass();
	
	if(loginForm.length == 1){
		document.loginForm.redirectPath.value = _redirectPath;
		document.loginForm.submit();
	} else {
		document.loginForm[0].redirectPath.value = _redirectPath;
		document.loginForm[0].submit();
	}
	
}

// redirectPassを作成します
function getRedirectPass(){
    var _location = location;
    var _redirectPath = _location.protocol + "//" + _location.host + _location.pathname;
    if (_location.search != null && _location.search != "") {
        _redirectPath += _location.search
    } else {
        var queryString = getQueryString();
        if (queryString != null) {
            _redirectPath += queryString;
        }
    }
    return _redirectPath;
}

// window.self.xxxForm.queryStringを検索しています
function getQueryString() {
	var forms = window.self.document.forms;
	if (forms == null) {
		return null;
	}
	for (var i = 0; i < forms.length; i++) {
		if (forms[i].name.match(/Form$/)) {
			var elements = forms[i].elements;
			for (var j = 0; j < elements.length; j++) {
				if (elements[j].id == "queryString") {
					return elements[j].value;
				}
			}
		}
	}
	return null;
}

function cartPageLinkAction(){
    var _redirectPath = getRedirectPass();
    document.cartPageLinkForm.redirectPath.value = _redirectPath;
    document.cartPageLinkForm.submit();
}
