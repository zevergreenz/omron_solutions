// -------------------------------------------------------------
// アンカー（hash）で指定されたIDの表示on/offを制御するJavaScript　2011/05/10
// MenuDisplayObj.id_make("メニューid", "初期番号", ["アクティブ時のclass指定","省略可能"]) 
// -------------------------------------------------------------
var MenuDisplayObj = new Object();
MenuDisplayObj = {
	// -------------------------------------------------------------
	// 情報の生成
	//  → 指定されたIDのaタグからhashを抜きだし、aタグにonclickイベントを設定
	// -------------------------------------------------------------
	id_make: function(id_str, def_num, class_ary) {
		// 配列宣言
		this.id_ary = new Array();

		// 情報取得
		var obj_a = document.getElementById(id_str).getElementsByTagName('a');
		for (i=0; i<obj_a.length; i++) {

			// ID取得
			var temp_id = obj_a[i].hash.split('#')[1];
			this.id_ary.push(temp_id);

			// hash変数で初期表示を変える
			if (obj_a[i].hash == location.hash) {
				def_num = i;
			}

			// onclickイベントの書込
			obj_a[i].onclick = function() {
				MenuDisplayObj.active_element(id_str,MenuDisplayObj.id_ary,this,class_ary);
				return false;
			};
		}
		// 最初の動作
		this.active_element(id_str,this.id_ary,obj_a[def_num],class_ary,true);
	},

	// -------------------------------------------------------------
	// click時のアクション
	// -------------------------------------------------------------
	active_element: function(id_str, in_ary, in_obj, inclass_ary,flag) {

		// hashからIDを取得
		var press_id = in_obj.hash.split('#')[1];

		// IDを表示/非表示の切替
		for (i=0; i<in_ary.length; i++) {
			if (document.getElementById(in_ary[i]) != null) {
				if (in_ary[i] == press_id) {
					document.getElementById(in_ary[i]).style.display = 'block';
				} else {
					document.getElementById(in_ary[i]).style.display = 'none';
				}
			}
		}

		// クリックしたオブジェクトの体裁を変える
		var obj_a = document.getElementById(id_str).getElementsByTagName('a');
		for (i=0; i<obj_a.length; i++) {
			// クリックしたもの以外
			if (inclass_ary != undefined) {
				obj_a[i].className = inclass_ary[0];
			} else {
				//obj_a[i].style.fontWeight = 'normal';
				obj_a[i].style.textDecorationUnderline = 'false';
				obj_a[i].style.fontSize = '100%';
				obj_a[i].style.backgroundColor = '#FFFFFF';
			}

		}
		// クリックされた文字。
		if (inclass_ary != undefined) {
			in_obj.className = inclass_ary[1];
		} else {
			//in_obj.style.fontWeight = 'bold';
			in_obj.style.textDecorationNone = 'false';
			in_obj.style.fontSize = '100%';
			in_obj.style.backgroundColor = '#d3dae5';
		}

		// 初回のみ動作、hashがある場合に表示位置がずれる対処。
		if (flag) {
			window.scroll(0,0);
		}

		// 
		return false;
	}
}