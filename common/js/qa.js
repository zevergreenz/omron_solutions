  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12070499-1']);
  _gaq.push(['_setDomainName', '.fa.omron.co.jp']);
  _gaq.push(['_addOrganic', 'search.ia.omron.com', 'q', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
  })();