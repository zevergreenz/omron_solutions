// #############################################################################
// Global Navigation
// #############################################################################

// change background color
$(function(){
    var url = document.URL.split('/');
    if ( url[3] == 'selection_guide' ) {
        $('#products').attr('id', 'current-navi');
    }
    if ( url[3] == 'new' ) {
        $('#products').attr('id', 'current-navi');
    }
    if ( url[3] == 'rohs' ) {
        $('#oep_search').attr('id', 'current-navi');
    }
    if ( url[3] == 'replacement' ) {
        $('#oep_search').attr('id', 'current-navi');
    }
    if ( url[3] == 'stock_info' ) {
        $('#oep_search').attr('id', 'current-navi');
    }
    if ( url[3] == 'search' ) {
        $('#products').attr('id', 'current-navi');
    }
    if ( url[3] == '' ) {
        $('#home').attr('id', 'current-navi');
    } else {
        $('.topMenu li').each(function() {
            var menu_id = $(this).attr('id');
            if (menu_id == url[3]) {
                $(this).attr('id', 'current-navi');
            return false;
            }
        });
    }
});


// For Australia
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.com.au' ) {
        $("a#country").text("Australia");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});

// For India
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.co.in' ) {
        $("a#country").text("India");
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});

// For Indonesia
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.co.id' ) {
        $("a#country").text("Indonesia");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
        $("a[href = '/about_us/careers/why_join_omron.asp']").parent().remove();
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});

// For Malaysia
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com.my' ) {
        $("a#country").text("Malaysia");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
    }
});

// For New Zealand
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.co.nz' ) {
        $("a#country").text("New Zealand");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.co.nz' ) {
        $("a#country").text("New Zealand");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});


// For Singapore
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com' ) {
        $("a#country").text("Singapore");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
    }
});
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com.sg' ) {
        $("a#country").text("Singapore");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
    }
});


// For Philippines
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com.ph' ) {
        $("a#country").text("Philippines");
        $("a[href = '/solutions/SMA/main.asp']").parent().remove();
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});

// For Thailand
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.co.th' ) {
        $("a#country").text("Thailand");
        $("a[href = '/solutions/one-stop_solution/main.asp']").parent().remove();
    }
});

// For Vietnam
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.com.vn' ) {
        $("a#country").text("Vietnam");
        $("a[href = '/stock_info/main.asp']").parent().remove();
    }
});


// #############################################################################
// Header / Footer
// #############################################################################
// Login URL
$(function(){
    var url = document.URL.split('/');
    var url2 = url[2];
    $("a[href = 'admin/login.asp']").attr("href", "https://" +url2+ "/admin/login.asp");
});


// ############################
// function : login URL hide show 
// add : 20140204
// ############################

//$(function(){
//	var setLogin = new iniLogin();
//	if (setLogin.truth) {
//        $("a.loginlink").remove();
//    }
//});


// #############################################################################
// Google Search
// #############################################################################
// For Australia
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.com.au' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "au");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_australia");
    }
});

// For India
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.co.in' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "in");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_india");
    }
});

// For Indonesia
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.co.id' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "id");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_indonesia");
    }
});

// For Malaysia
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com.my' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "my");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_malaysia");
    }
});

// For New Zealand
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.co.nz' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "nz");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_new_zealand");
    }
});
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.co.nz' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "nz");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_new_zealand");
    }
});

// For Singapore
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "sg");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_new_singapore");
    }
});
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com.sg' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "sg");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_new_singapore");
    }
});

// For Philippines
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.com.ph' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "ph");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_philippines");
    }
});

// For Thailand
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron-ap.co.th' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "th");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_thailand");
    }
});

// For Vietnam
$(function(){
    var url = document.URL.split('/');
    if ( url[2] == 'www.omron.com.vn' ) {
        $("form#search_website input[value = 'country-code']").attr("value", "vn");
        $("form#search_website input[value = 'country-site']").attr("value", "gs_vietnam");
    }
});
