var iniLogin = function() {
	this.key01 = "username";
	this.key02 = "username";
	this.key01Val;
	this.key02Val;
	//this.testData();
	this.isLogin();
};

iniLogin.prototype = {

	// ログイン判定
	isLogin: function() {
		this.key01Val = this.getCookie(this.key01);
		this.key02Val = this.getCookie(this.key02);
		if (this.key01Val != "" && this.key02Val != "") {
			return this.truth = true;
		}
		else {
			return this.truth = false;
		}
	},

	// クッキー読込
	getCookie: function(name) {
		readVal = '';
		name += '='
		data = document.cookie + ';';
		start = data.indexOf(name);
		if (start != -1) {
			end = data.indexOf(";", start);
			readVal = unescape(data.substring(start + name.length, end));
		}
		return readVal;
	},

	// クッキー書込
	setCookie: function(name, val, tm) {
		var now = new Date();
		now.setTime(now.getTime() + (tm * 1000 * 60 * 60 * 24));
		var expDay = now.toGMTString();
		document.cookie = name + "=" + escape(val) + ";expires=" + expDay;
	},

	// クッキー削除
	delCookie: function(name) {
		document.cookie = name + "=;expires=Thu,01-Jan-70 00:00:01 GMT";
	},

	// クッキーセット（テスト用）
	testData: function() {
		var q = location.search.split("?");
		if (q[1] == 1) {
			this.setCookie(this.key01, "11223344", 1);
			this.setCookie(this.key02, "55667788", 1);
		}
		else {
			this.delCookie(this.key01);
			this.delCookie(this.key02);
		}
	}

}
